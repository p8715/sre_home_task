# SRE_Home_Task

1.	Build a custom Jenkins Docker image from the official base 
Install role based auth strategy plugin if not there https://plugins.jenkins.io/role-strategy
Enable the following roles:
    1.	Admin
    2.	Deployer
    3.	Developer
    4.	Prod-deployer
Give relevant access to the above roles
Save image back on Docker hub
Definition of done: 
The resultant image in Docker hub
Rationale applied in giving access to each of the roles. 



## Assumptions

    - Selected Debian docker base image as no preference was mentioned
    - Role Based strategy plugin was selected as we can customize the members in the team in accessing the projects
    

## Commands used to build the Docker image on Amazon EC2 instance.


```
systemctl status docker
systemctl start docker
[root@Docker_Server ~]# docker images
REPOSITORY        TAG         IMAGE ID       CREATED        SIZE
sre_jenkins       latest      b4c3328f033f   2 hours ago    502MB
jenkins/jenkins   lts-jdk11   fd576e09d155   12 days ago    464MB
centos            7           eeb6ee3f44bd   7 months ago   204MB
centos            latest      5d0da3dc9764   7 months ago   231MB
[root@Docker_Server ~]#

#Pull the latest image from hub
docker push snatesa1/sre_image_20022022:tagname


```

## User Creation and Role Management

![image.png](./image.png)
![image-1.png](./image-1.png)
![image-2.png](./image-2.png)
![image-3.png](./image-3.png)

## Role Mappings
There are some issues with the plugin while creating the users without type:name.
So seeing the type issues while creating the users.


![image-4.png](./image-4.png)

## Projects for testing the Role based Authorization

Role based access was granted to isolate the projects from each members based on their roles in the firm.
For example javaDeveloper was granted access to only the Java projects and not for the kdb projects.
Similarly for the QA role the access was granted for the testers.

![image-5.png](./image-5.png)
![image-6.png](./image-6.png)


## Create a pipeline to automate and publish the above image after testing the relevant parts.

a. Pipeline in chosen tool
Choosed Gitlab runner as a pipeline tool because of the following reasons.
1.Modern tool with lot of integration features.
2.Highly scalable
3.Docker first and we have inbuilt container registary and repository
4.Easy integration with most container orchestration tools like (argocd etc)
        
b. Test cases.
        ![image-7.png](./image-7.png)

Above is the .gitlab.yml configuration pipeline for the gitlab runner process and I haven't added more test cases as I was not having more handson Jenkins testing specific to the role authorization.From pipeline standpoint we can have many stages before we push to Production.I added only build and test.

## Run the above image on a hosted Docker service. 

URL of the running service 
 ```
    [root@ip-172-31-23-91 ~]# docker service create --name jenkins_sre_take_home snatesa1/sre_image_20022022:latest
m2tb7wedvo4dj8iquckoapxbe
overall progress: 1 out of 1 tasks
1/1: running   [==================================================>]
verify: Service converged
[root@ip-172-31-23-91 ~]# docker service ls
ID             NAME                    MODE         REPLICAS   IMAGE                                PORTS
m2tb7wedvo4d   jenkins_sre_take_home   replicated   1/1        snatesa1/sre_image_20022022:latest
[root@ip-172-31-23-91 ~]#

```
-Rationale behind choosing the hosted service if any.
 To ensure Jenkins service is running after the OS restart.

***

