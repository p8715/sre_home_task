FROM jenkins/jenkins:lts-jdk11
USER root
RUN apt update && \
    apt install -y --no-install-recommends gnupg curl ca-certificates apt-transport-https
RUN jenkins-plugin-cli --plugins role-strategy:3.2.0
VOLUME /var/jenkins_home
USER jenkins

